export default {
    items: [
        {
            id: 'navigation',
            title: 'Navigation',
            type: 'group',
            icon: 'icon-navigation',
            children: [
                {
                    id: 'contacts',
                    title: 'Contacts',
                    type: 'collapse',
                    icon: 'feather icon-home',
                    children: [
                        {
                            id: 'contact-category',
                            title: 'Contact Category',
                            type: 'item',
                            url: '/contact/category'
                        },                        
                        {
                            id: 'add-contact-category',
                            title: 'Add Category',
                            type: 'item',
                            url: '/add/category'
                        },
                        {
                            id: 'project',
                            title: 'Project',
                            type: 'item',
                            url: '/dashboard/project'
                        }
                    ]
                },
                {
                    id: 'invoice',
                    title: 'Invoice',
                    type: 'collapse',
                    icon: 'feather icon-file-minus',
                    children: [
                        {
                            id: 'invoice-basic',
                            title: 'Invoice Basic',
                            type: 'item',
                            url: '/invoice/invoice-basic'
                        },
                        {
                            id: 'invoice-summary',
                            title: 'Invoice Summary',
                            type: 'item',
                            url: '/invoice/invoice-summary'
                        },
                        {
                            id: 'invoice-list',
                            title: 'Invoice List',
                            type: 'item',
                            url: '/invoice/invoice-list'
                        },
                        {
                            id: 'bootstrap',
                            title: 'Bootstrap Table',
                            type: 'item',
                            url: '/tables/bootstrap'
                        },
                        {
                            id: 'data-table',
                            title: 'Data Tables',
                            type: 'item',
                            url: '/tables/datatable'
                        }
                    ]
                },                
                {
                    id: 'employees',
                    title: 'Employees',
                    type: 'collapse',
                    icon: 'feather icon-layers',
                    children: [
                        {
                            id: 'user-list',
                            title: 'Employee List',
                            type: 'item',
                            url: '/users/user-list'
                        }
                    ]                    
                },
                {
                    id: 'services-products',
                    title: 'Services/Products',
                    type: 'collapse',
                    icon: 'feather icon-layers'                   
                },
                {
                    id: 'sales',
                    title: 'Sales',
                    type: 'collapse',
                    icon: 'feather icon-layers'                   
                },
                {
                    id: 'income-expense',
                    title: 'Income/Expense',
                    type: 'collapse',
                    icon: 'feather icon-home' ,
                    target: true                   
                },                
                {
                    id: 'task-management',
                    title: 'Task Management',
                    type: 'collapse',
                    icon: 'feather icon-home' ,
                    target: true                   
                },
                {
                    id: 'campaign-management',
                    title: 'Campaign Mgnt',
                    type: 'collapse',
                    icon: 'feather icon-home' ,
                    target: true                   
                },
                {
                    id: 'customer-support',
                    title: 'Customer Support',
                    type: 'collapse',
                    icon: 'feather icon-home' ,
                    target: true                   
                },
                {
                    id: 'email-sms',
                    title: 'Email and SMS',
                    type: 'collapse',
                    icon: 'feather icon-home' ,
                    target: true                   
                },
                {
                    id: 'reports',
                    title: 'Reports',
                    type: 'collapse',
                    icon: 'feather icon-home' ,
                    target: true                   
                },
                {
                    id: 'users',
                    title: 'Users',
                    type: 'collapse',
                    icon: 'feather icon-home' 
                },
                {
                    id: 'settings',
                    title: 'Settings',
                    type: 'collapse',
                    icon: 'feather icon-home' 
                }
            ]
        }
    ]
}