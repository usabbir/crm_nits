import React from 'react';
import {Row, Col, Card, Table} from 'react-bootstrap';
import axios from 'axios';

import Aux from "../../hoc/_Aux";
import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;


// $.DataTable = require( 'datatables.net-bs' );
// require( 'datatables.net-responsive-bs' );

// function atable() {
//     let tableZero = '#data-table-zero';
//     $.fn.dataTable.ext.errMode = 'throw';

//     $(tableZero).DataTable( {
//         data: fetch('/api/web/v1/contact-category/?access-token=9e74904aa7581d08c1465f8fc842630c').then(res=>res.data),
//         order: [[ 0, "asc" ]],
//         columns: [
//             { "data": "id", render: function (data, type, row) {return data;}},
//             { "data": "name", render: function (data, type, row) {return data;}},
//             { "data": "created_at", render: function (data, type, row) {return data;}},
//             { "data": "created_at", render: function (data, type, row) {return data;}},
//         ]
//     });            
// }

class ContactCategory extends React.Component {

    constructor(){
        super();
        this.state={
            categories: []
        }
    }

    componentDidMount(){
        // atable()
        // const headers = {
        //     'Content-Type': 'application/json;charset=UTF-8',
        //     "Access-Control-Allow-Origin": "*",
        // }
        // var config = {
        //     headers: {"Access-Control-Allow-Origin": "*"}
        // }        
        axios.get('/api/web/v1/contact-category/?access-token=9e74904aa7581d08c1465f8fc842630c') //get base path from package.json proxy
        .then(response=>{
            this.setState({categories: response.data.data})
        })
    }

    render() {
        console.log(this.state.categories);
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">Zero Configuration</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <Table ref="tbl" striped hover responsive bordered id="data-table-zero">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Created At</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.categories.map((category, index)=>{return (
                                        <tr key={index}>
                                            <td>{category.id}</td>
                                            <td>{category.name}</td>
                                            <td>{category.created_at}</td>
                                            <td>action</td>
                                        </tr>
                                        )})}
                                    </tbody>
                                </Table>
                            </Card.Body>
                        </Card>                        
                        
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default ContactCategory;