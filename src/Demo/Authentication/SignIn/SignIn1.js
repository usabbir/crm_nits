import React from 'react';
import { NavLink } from 'react-router-dom';

import './../../../assets/scss/style.scss';
import Aux from "../../../hoc/_Aux";
import Breadcrumb from "../../../App/layout/AdminLayout/Breadcrumb";
import logoDark from '../../../assets/images/logo-dark.png';
import axios from 'axios';

class SignUp1 extends React.Component {
    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            is_invalid_user: false,
        }
        this.handleChange = this.handleChange.bind(this);
        this.handelSubmit = this.handelSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    handelSubmit(event) {
        event.preventDefault();
        const headers = {
            'Content-Type': 'application/json;charset=UTF-8',
            "Access-Control-Allow-Origin": "*",
        }
        axios.post('http://crmapi.nesteditsolutions.com/api/web/v1/user/login/?access-token=9e74904aa7581d08c1465f8fc842630c', {
            username: this.state.username,
            password: this.state.password,
        })
            .then(res => {
                console.log(res.data);
                // this.setState({ is_invalid_user: false });

                // localStorage.setItem('app_token', res.data.success.token);



                // const requestOptions = {
                //     method: 'GET',
                //     headers: {
                //         'Content-Type': 'application/json',
                //         'Accept': 'application/json',
                //         "Authorization": "Bearer " + res.data.success.token,
                //     },
                // };
                // fetch(BASE_URL + '/api/my-profile-details', requestOptions)
                //     .then(Response => Response.json())
                //     .then(res => {
                //         res.success.profile.id = '';
                //         res.success.profile.user_id = '';
                //         localStorage.setItem(APP_MY_PROFILE_NAME, JSON.stringify(res.success.profile));
                //         window.location.href = "/dashboard";

                //     })
                //     .catch(err => {
                //         console.log(err);
                //         window.location.href = "/";
                //     });


            })
            .catch(err => {
                this.setState({ is_invalid_user: true })
                this.setState({ isLoading: false });
                this.setState({ errors: err.response.data.error })
            });
    }

    render() {
        return (
            <Aux>
                <Breadcrumb />
                <div className="auth-wrapper">
                    <div className="auth-content">
                        <div className="card">
                            <div className="row align-items-center text-center">
                                <div className="col-md-12">
                                    <div className="card-body">
                                        <form onSubmit={this.handelSubmit}>
                                            <img src={logoDark} alt="" className="img-fluid mb-4" />
                                            <h4 className="mb-3 f-w-400">Signin</h4>
                                            <div className="input-group mb-3">
                                                <input type="text" name="username" value={this.state.username} className="form-control" onChange={this.handleChange} placeholder="Email address" />
                                            </div>
                                            <div className="input-group mb-4">
                                                <input type="password" name="password" value={this.state.password} className="form-control" onChange={this.handleChange} placeholder="Password" />
                                            </div>
                                            <button type="submit" className="btn btn-block btn-primary mb-4">Signin</button>
                                            <p className="mb-2 text-muted">Forgot password? <NavLink to="/auth/reset-password-1" className="f-w-400">Reset</NavLink></p>
                                            <p className="mb-0 text-muted">Don’t have an account? <NavLink to="/auth/signup-1" className="f-w-400">Signup</NavLink></p>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Aux>
        );
    }
}

export default SignUp1;