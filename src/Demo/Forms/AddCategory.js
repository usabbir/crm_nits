import React, { Component } from 'react';
import {Row, Col, Card, Form, Button} from 'react-bootstrap';
import { ValidationForm, TextInput, BaseFormControl, SelectGroup, FileInput, Checkbox, Radio } from 'react-bootstrap4-form-validation';

import Aux from "../../hoc/_Aux";
import axios from 'axios';

class AddCategory extends Component {
    constructor(){
        super(); 
        this.state = {
            ContactCategoryName:  ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handelSubmit = this.handelSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    handelSubmit(event) {
        event.preventDefault();
        axios.post('/api/web/v1/contact-category/create?access-token=9e74904aa7581d08c1465f8fc842630c', {
            name: this.state.ContactCategoryName
        })
        .then(res=>console.log(res.data))
        .catch(function(error){
            console.log(error);
        })
    }
    

    render() {
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">Add Category</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <ValidationForm onSubmit={this.handelSubmit}>
                                    <Form.Row>
                                        <Form.Group as={Col} md="12">
                                            <Form.Label htmlFor="firstName">Category name</Form.Label>
                                            <TextInput
                                                name="ContactCategoryName"
                                                id="firstName"
                                                placeholder="Category Name"
                                                // required value={this.state.firstName}
                                                autoComplete="off"
                                                onChange={this.handleChange}
                                            />
                                        </Form.Group>
                                        <Form.Group as={Col} sm={12} className="mt-3">
                                            <Button type="submit">Submit</Button>
                                        </Form.Group>
                                    </Form.Row>
                                </ValidationForm>                                
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default AddCategory;